### Hi there 👋

<!--
**hadpro24/hadpro24** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

Here are some ideas to get you started:

- 🔭 I’m currently working on ...
- 🌱 I’m currently learning ...
- 👯 I’m looking to collaborate on ...
- 🤔 I’m looking for help with ...
- 💬 Ask me about ...
- 📫 How to reach me: ...
- 😄 Pronouns: ...
- ⚡ Fun fact: ...
 [![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=hadpro24)](https://github.com/anuraghazra/github-readme-stats)
-->

- 🔭 I’m currently building **Nimba** 
- 🌱 I’m currently learning **Self Supervised Learning** and **Rust**
- 👯 I’m looking to collaborate on python (Django and JavaScript), preferences from Guinea
- 🤔 I’m looking for help with Self Supervised Learning
- 💬 Ask me about Python/API GateWay/Message brokers/JavaScript/Error tracking/OAuth 2.0 and OpenID connect/SSO/CI-CD/Github Actions/Rate liming/NLP/Machine Learning and APEX (salesforce)
- 📫 How to reach me: dev.harouna@gmail.com

<!-- 
 ![Harouna's github stats](https://github-readme-stats.vercel.app/api?username=hadpro24&show_icons=true&theme=radical)
 -->
 
 
### My Meet-up, Live coding and Talks
- **2022** - [Machine Learning et Deploiement](https://docs.google.com/presentation/d/1jvnBrSlwOymGZzQ4hnodA4Hk8GHcdqSghUZoIdQ1qH8/edit?usp=sharing) (ORANGE DIGITAL CENTER - Place)
- **2022** - [NLP - Traitement automatique des langues](https://docs.google.com/presentation/d/1JMRU3QQCHkQL-BRJj8qiwCQ6DXoCeOOzlkIO894QFsc/edit?usp=sharing) (Google Data Science Week - Online)
- **2021** - [Déléguez vos authentifications avec OAuth 2.0 et OpenID connect](https://docs.google.com/presentation/d/1rfdFL-QLsqFkc9cpP18LM-MgRjhqqTR4kBM75PRTgBU/edit#slide=id.gc6f73a04f_0_0) (Google developers Groups - Online)
- **2020** - [Créer votre premier modèle d'apprentissage automatique](https://docs.google.com/presentation/d/1N138W9tS7yvkJs7kX6TbRzQJLPivoQWgu1XDJbA0DzE/edit#slide=id.gcb9a0b074_1_0) (Google Data Science Week - Online)
- **2019** - [Comprendre enfin les APIs et micro-services](https://docs.google.com/presentation/d/10kd9rxFrfCuzxAPH4m_ErmZbztRIu0mDDJvb1AOu-Mw/edit?usp=sharing) (DevsCom - Place)
- **2019** - [Web programming with python and JavaScript - Edition 3](https://docs.google.com/presentation/d/1_Z_TZdAPajrugbz66RThXEt1AOt7RqE1/edit#slide=id.p1) (Nimba Solution - Place)
- **2018** - [Web programming with python and JavaScript - Edition 2](https://docs.google.com/presentation/d/1GXSmDdLj0Q2nKC7s5Z432gu37HQjnnVq/edit#slide=id.p1)  (CEUG - Place)
- **2017** - [Web programming with python and JavaScript - Edition 1](https://docs.google.com/presentation/d/1au-MRGxzXwXbOuQJrT06Cm3IW-Sq2zXr/edit?usp=sharing&ouid=113016605800835600757&rtpof=true&sd=true) (Village d'opportunités TOogueda - Place)

### Stats :
<!-- [![Harouna's github activity graph](https://activity-graph.herokuapp.com/graph?username=hadpro24&theme=xcode)](https://github.com/hadpro24) -->

![](https://github-profile-summary-cards.vercel.app/api/cards/profile-details?username=hadpro24&theme=solarized_dark) 

